package teemo.visitor_amp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by TeEM0 on 2017-08-10.
 */

public class SplashActivity extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            Thread.sleep(0);
        }catch(Exception e){
            e.printStackTrace();
        }
        startActivity(new Intent(getApplication(),MainActivity.class));
        finish();
    }
}
