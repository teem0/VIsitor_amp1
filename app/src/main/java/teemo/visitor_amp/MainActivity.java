package teemo.visitor_amp;

import android.net.http.SslError;
import android.os.Handler;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringDef;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {


    EditText minTime, maxTime, repetition, URL;
    Button execute;
    WebView wv;

    int minTime_int,maxTime_int,repetition_int,time;
    int counter;
    String URL_str;

    //For Scrolling Down
    private Handler mHandler;
    private Runnable r_wv;
    private Runnable r_wv_wrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        //Connecting each elements in view to elements
        minTime = (EditText) findViewById(R.id.minTime);
        maxTime = (EditText) findViewById(R.id.maxTime);
        repetition = (EditText) findViewById(R.id.repetition);
        URL = (EditText) findViewById(R.id.URL);
        execute = (Button) findViewById(R.id.execute);
        wv = (WebView) findViewById(R.id.webView);
        mHandler = new Handler();

        wv.getSettings().setJavaScriptEnabled(true);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                //to be fixed later For now, well yee~
                handler.proceed();
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                r_wv = new Runnable() {
                    public void run() {
                        wv.scrollBy(0, wv.getContentHeight()/(time*10));
                        mHandler.postDelayed(this, time*80);
                        Toast.makeText(MainActivity.this, String.valueOf(wv.getScrollY())+"yeyeyey", Toast.LENGTH_SHORT).show();
                        if(wv.getScrollY() >= wv.getContentHeight()){
                            mHandler.removeCallbacksAndMessages(this);
                        }
                    }
                };
                r_wv.run();
            }
        });
        execute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    minTime_int=Integer.valueOf(minTime.getText().toString());
                    maxTime_int=Integer.valueOf(maxTime.getText().toString());
                    repetition_int=Integer.valueOf(repetition.getText().toString());
                    URL_str=URL.getText().toString();
                    counter =0;
                    //check Int
                    if(checkInt(minTime_int,maxTime_int) && checkRep(repetition_int)) {
                        //check URL
                        if (Patterns.WEB_URL.matcher(URL_str).matches()) {
                            if (!URL_str.contains("http://") || !URL_str.contains("https://"))
                                URL_str = "http://" + URL_str;

                            //_____________________________________________________________________________________________________repeat;
                            final Random r = new Random();

                            r_wv_wrapper=new Runnable() {
                                @Override
                                public void run() {
                                    //bring random int!
                                    //load url
                                    /*To do
                                    * While(Count < Repeat)
                                    *   Draw Random int in the range
                                    *   Load URL,
                                    *   If finished Loading URL, Start To scroll the page
                                    *     else failed, Stop all
                                    *   If Scoll reaches to the end of page,
                                    *     reload the page
                                    * */
                                    time = r.nextInt(maxTime_int - minTime_int) + minTime_int;

                                    wv.loadUrl(URL_str);

                                    if(counter >= repetition_int) {
                                        mHandler.removeCallbacksAndMessages(this);
                                        Toast.makeText(getApplicationContext(),"Finished all jobs",Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(getApplicationContext(),String.valueOf(time*wv.getContentHeight()*1000),Toast.LENGTH_SHORT).show();
                                        mHandler.removeCallbacksAndMessages(null);
                                        mHandler.postDelayed(this,time*wv.getContentHeight()*1000);
                                        counter++;
                                    }
                                }
                            };
                            mHandler.post(r_wv_wrapper);
                            //_____________________________________________________________________________________________________
                        } else {
                            Toast.makeText(getApplicationContext(), "URL형식확인요망", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "최소값이 더 큽니다", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(),"모든 칸을 입력해주세요 ㅠㅜ",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
    }

    public boolean checkInt(int min, int max){
        return max >= min;
    }
    public boolean checkRep(int rep){
        return rep > 0;
    }
}
